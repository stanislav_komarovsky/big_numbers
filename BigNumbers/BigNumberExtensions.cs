﻿using System;

namespace BigNumbers
{
    public static class BigNumberExtensions
    {
        public static string AsFullString(this Unit unit)
        {
            return unit == Unit.Simple ? string.Empty : unit.ToString();
        }

        public static string AsShortString(this Unit unit)
        {
            switch (unit)
            {
                case Unit.Simple:
                    return string.Empty;
                case Unit.Thousand:
                    return "K";
                case Unit.Million:
                    return "M";
                case Unit.Billion:
                    return "B";
                case Unit.Trillion:
                    return "T";
                case Unit.Quadrillion:
                    return "q";
                case Unit.Quintillion:
                    return "Q";
                case Unit.Sextillion:
                    return "s";
                case Unit.Septillion:
                    return "S";
                case Unit.Octillion:
                    return "O";
                case Unit.Nonillion:
                    return "N";
                case Unit.Decillion:
                    return "D";
                case Unit.Undecillion:
                    return "U";
                default:
                    throw new ArgumentOutOfRangeException(nameof(unit), unit, null);
            }
        }

        public static string AsString(this Unit unit, UnitFormat unitFormat)
        {
            switch (unitFormat)
            {
                case UnitFormat.Short:
                    return unit.AsShortString();
                case UnitFormat.Full:
                    return unit.AsFullString();
                default:
                    throw new ArgumentOutOfRangeException(nameof(unitFormat), unitFormat, null);
            }
        }
    }
}