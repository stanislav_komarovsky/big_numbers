﻿namespace BigNumbers
{
    public enum Unit
    {
        Simple,
        Thousand,
        Million,
        Billion,
        Trillion,
        Quadrillion,
        Quintillion,
        Sextillion,
        Septillion,
        Octillion,
        Nonillion,
        Decillion,
        Undecillion
    }
}