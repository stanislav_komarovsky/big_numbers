﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BigNumbers
{
    public class BigNumber
    {
        private const ushort MaxUnitValue = 999;
        private const ushort MaxUnitValueEx = MaxUnitValue + 1;
        private const byte MaxUnitValueDigitsCount = 3;
        private const string ValueFractionSeparator = ".";
        private const string ValueGroupSeparator = ",";
        private const string FullStringFormat = "{0} {1}";
        private const string ShortStringFormat = "{0}{1}";
        private static readonly Array UnitValues = Enum.GetValues(typeof(Unit));
        private static readonly int UnitsCount = Enum.GetValues(typeof(Unit)).Length;
        private static readonly Unit MaxPossibleUnit = (Unit)UnitValues.GetValue(UnitsCount - 1);
        private static readonly Unit MinPossibleUnit = (Unit)UnitValues.GetValue(0);

        private readonly Dictionary<Unit, ushort> _parts = new Dictionary<Unit, ushort>(UnitsCount);

        private ushort this[Unit unit]
        {
            get
            {
                ushort result;
                _parts.TryGetValue(unit, out result);
                return result;
            }
            set
            {
                _parts[unit] = Math.Min(value, MaxUnitValue);
                _maxUnit = null;
            }
        }

        private Unit? _maxUnit;
        public Unit MaxUnit
        {
            get
            {
                if (!_maxUnit.HasValue)
                {
                    var maxUnit = Unit.Simple;
                    for (var unit = MaxPossibleUnit; unit >= Unit.Simple; unit--)
                    {
                        if (this[unit] != 0)
                        {
                            maxUnit = unit;
                            break;
                        }
                    }

                    _maxUnit = maxUnit;
                }

                return _maxUnit.Value;
            }
        }

        private bool IsNegative { get; set; }

        public BigNumber()
        {
        }

        public BigNumber(BigNumber number) : this()
        {
            foreach (var part in number._parts)
            {
                _parts.Add(part.Key, part.Value);
            }
            IsNegative = number.IsNegative;
        }

        public BigNumber(long number) : this()
        {
            if (number == 0)
            {
                return;
            }
            if (number < 0)
            {
                IsNegative = true;
                number = Math.Abs(number);
            }

            var unit = Unit.Simple;
            while (number != 0)
            {
                var part = number % MaxUnitValueEx;
                this[unit] = (ushort) part;
                number = number / MaxUnitValueEx;
                unit++;
            }
        }

        public BigNumber(string number) : this()
        {
            number = number.Trim();

            var firstIndex = 0;

            if (number[0] == '-')
            {
                IsNegative = true;
                firstIndex = 1;
            }

            var unit = Unit.Simple;
            for (var i = number.Length - MaxUnitValueDigitsCount; i > firstIndex - MaxUnitValueDigitsCount; i -= MaxUnitValueDigitsCount)
            {
                var part = i < firstIndex
                    ? number.Substring(firstIndex, MaxUnitValueDigitsCount - (firstIndex - i))
                    : number.Substring(i, Math.Min(MaxUnitValueDigitsCount, number.Length - i));

                ushort partNumber;
                if (ushort.TryParse(part, out partNumber))
                {
                    this[unit] = partNumber;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(number), number, null);
                }

                unit++;
            }
        }

        public override string ToString()
        {
            return ToString(MaxUnit);
        }

        public string ToString(UnitFormat unitFormat)
        {
            return ToString(MaxUnit, unitFormat);
        }

        public string ToString(Unit shortenStartingFrom, UnitFormat unitFormat = UnitFormat.Short)
        {
            var maxUnit = MaxUnit >= shortenStartingFrom
                ? shortenStartingFrom
                : Unit.Simple;
            var valueString = GetValueString(maxUnit);
            var formatString = GetFormatString(unitFormat);

            if (maxUnit == Unit.Simple)
            {
                return valueString;
            }

            return string.Format(formatString, valueString, maxUnit.AsString(unitFormat));
        }

        private string GetFormatString(UnitFormat unitFormat)
        {
            switch (unitFormat)
            {
                case UnitFormat.Short:
                    return ShortStringFormat;
                case UnitFormat.Full:
                    return FullStringFormat;
                default:
                    throw new ArgumentOutOfRangeException(nameof(unitFormat), unitFormat, null);
            }
        }

        public static implicit operator BigNumber(long number)
        {
            return new BigNumber(number);
        }

        public static explicit operator BigNumber(string number)
        {
            return new BigNumber(number);
        }

        public static explicit operator long(BigNumber number)
        {
            var mult = 1;
            long result = 0;
            for (var unit = Unit.Simple; unit <= MaxPossibleUnit; unit++)
            {
                result += number[unit]*mult;
                mult *= MaxUnitValueEx;
            }
            if (number.IsNegative)
            {
                result *= -1;
            }
            return result;
        }

        public static implicit operator string(BigNumber number)
        {
            return number.ToString();
        }

        public BigNumber Abs => new BigNumber(this) {IsNegative = false};

        private string GetValueString(Unit maxUnit)
        {
            var stringBuilder = new StringBuilder(IsNegative ? "-" : string.Empty);

            for (var unit = MaxUnit; unit >= maxUnit; unit--)
            {
                if (unit == MaxUnit)
                {
                    stringBuilder.Append(this[unit]);
                }
                else
                {
                    stringBuilder.Append($"{ValueGroupSeparator}{this[unit]:D3}");
                }
            }

            if (maxUnit > Unit.Simple && this[maxUnit - 1] > 0)
            {
                stringBuilder.Append($"{ValueFractionSeparator}{this[maxUnit - 1]:D3}");
            }

            return stringBuilder.ToString();
        }

        public static BigNumber operator +(BigNumber n1, BigNumber n2)
        {
            if (n1 == null)
            {
                return n2;
            }
            if (n2 == null)
            {
                return n1;
            }
            if (n1.IsNegative && !n2.IsNegative)
            {
                return n2 - n1.Abs;
            }
            if (!n1.IsNegative && n2.IsNegative)
            {
                return n1 - n2.Abs;
            }
            if (n1.IsNegative && n2.IsNegative)
            {
                return -(n1.Abs + n2.Abs);
            }

            var result = new BigNumber(n1);

            for (var unit = Unit.Simple; unit <= MaxPossibleUnit; unit++)
            {
                result[unit] += n2[unit];

                if (result[unit] > MaxUnitValue && unit != MaxPossibleUnit)
                {
                    result[unit + 1] += (ushort) (result[unit] - MaxUnitValue);
                    result[unit] = MaxUnitValue;
                }
            }

            return result;
        }

        public static BigNumber operator -(BigNumber n1, BigNumber n2)
        {
            if (n1 == null)
            {
                return -n2;
            }
            if (n2 == null)
            {
                return n1;
            }
            if (n1.IsNegative && !n2.IsNegative)
            {
                return -(n1.Abs + n2);
            }
            if (!n1.IsNegative && n2.IsNegative)
            {
                return n1 + n2.Abs;
            }
            if (n1.IsNegative && n2.IsNegative)
            {
                return n2.Abs - n1.Abs;
            }
            if (n1 < n2)
            {
                return -(n2 - n1);
            }

            var result = new BigNumber(n1);

            for (var unit = Unit.Simple; unit <= MaxPossibleUnit; unit++)
            {
                var value = result[unit] - n2[unit];

                if (value < 0)
                {
                    value += MaxUnitValueEx;
                    result[unit + 1] -= 1;
                }

                result[unit] = (ushort) value;
            }

            return result;
        }

        public static BigNumber operator -(BigNumber n)
        {
            if (n == null)
            {
                return null;
            }

            return new BigNumber(n) {IsNegative = !n.IsNegative};
        }

        public static BigNumber operator *(BigNumber n1, BigNumber n2)
        {
            if (n1 == null || n2 == null)
            {
                return new BigNumber(0);
            }

            var result = new BigNumber();

            for (var unit2 = Unit.Simple; unit2 <= n2.MaxUnit; unit2++)
            {
                for (var unit1 = Unit.Simple; unit1 <= n1.MaxUnit; unit1++)
                {
                    var value = n1[unit1] * n2[unit2];
                    var number = new BigNumber(value);
                    for (var shift = unit1; shift > Unit.Simple; shift--)
                    {
                        number.ShiftUnitLeft();
                    }
                    for (var shift = unit2; shift > Unit.Simple; shift--)
                    {
                        number.ShiftUnitLeft();
                    }
                    result += number;
                }
            }

            result.IsNegative = n1.IsNegative != n2.IsNegative;

            return result;
        }

        public static BigNumber operator *(BigNumber n1, float n2)
        {
            if (n1 == null || Math.Abs(n2) < float.Epsilon)
            {
                return new BigNumber(0);
            }

            var isNegative = n2 < 0;
            n2 = Math.Abs(n2);

            var result = new BigNumber();

            for (var unit = Unit.Simple; unit <= n1.MaxUnit; unit++)
            {
                var value = n1[unit] * n2;
                var valueWhole = (long) value;
                var fraction = value - valueWhole;
                var number = new BigNumber(valueWhole);
                for (var shift = unit; shift > Unit.Simple; shift--)
                {
                    number.ShiftUnitLeft();
                    fraction *= 1000;
                    var fractionWhole = (long)fraction;
                    number += fractionWhole;
                    fraction -= fractionWhole;
                }
                result += number;
            }

            result.IsNegative = n1.IsNegative != isNegative;

            return result;
        }

        public static BigNumber operator *(float n1, BigNumber n2)
        {
            return n2 * n1;
        }

        public static BigNumber operator /(BigNumber n1, float n2)
        {
            return n1 * (1 / n2);
        }

        public static bool operator >(BigNumber n1, BigNumber n2)
        {
            if (ReferenceEquals(n1, n2))
            {
                return false;
            }
            if (n1 == null)
            {
                return n2.IsNegative;
            }
            if (n2 == null)
            {
                return !n1.IsNegative;
            }
            var equals = true;
            for (var unit = MaxPossibleUnit; unit >= Unit.Simple; unit--)
            {
                if (n1[unit] < n2[unit])
                {
                    return false;
                }
                if (n1[unit] != n2[unit])
                {
                    equals = false;
                }
            }

            return !equals;
        }

        public static bool operator <(BigNumber n1, BigNumber n2)
        {
            return n2 > n1;
        }

        public static bool operator ==(BigNumber n1, BigNumber n2)
        {
            if (ReferenceEquals(n1, n2))
            {
                return true;
            }
            if (ReferenceEquals(n1, null) || ReferenceEquals(n2, null))
            {
                return false;
            }
            if (n1.IsNegative != n2.IsNegative)
            {
                return false;
            }
            for (var unit = Unit.Simple; unit <= MaxPossibleUnit; unit++)
            {
                if (n1[unit] != n2[unit])
                {
                    return false;
                }
            }

            return true;
        }

        public static bool operator !=(BigNumber n1, BigNumber n2)
        {
            return !(n1 == n2);
        }

        protected bool Equals(BigNumber other)
        {
            if (ReferenceEquals(null, other)) return false;
            return this == other;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as BigNumber);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_parts.GetHashCode()*397) ^ IsNegative.GetHashCode();
            }
        }

        private void ShiftUnitLeft()
        {
            for (var unit = MaxUnit; unit >= Unit.Simple; unit--)
            {
                if (unit == MaxPossibleUnit)
                {
                    continue;
                }
                this[unit + 1] = this[unit];
            }
            this[Unit.Simple] = 0;
        }

        private void ShiftUnitRight()
        {
            var maxUnit = MaxUnit;
            for (var unit = Unit.Simple; unit <= maxUnit; unit++)
            {
                if (unit == Unit.Simple)
                {
                    continue;
                }
                this[unit - 1] = this[unit];
            }
            this[maxUnit] = 0;
        }
    }
}