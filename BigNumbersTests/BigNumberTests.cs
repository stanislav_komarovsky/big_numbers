﻿using BigNumbers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigNumbersTests
{
    [TestClass]
    public class BigNumberTests
    {
        [TestMethod]
        public void CreateBlank()
        {
            var number = new BigNumber();
            Assert.IsInstanceOfType(number, typeof(BigNumber));
        }

        [TestMethod]
        public void CreateFromLong()
        {
            Assert.AreEqual(123, (long)new BigNumber(123));
            Assert.AreEqual(-123, (long)new BigNumber(-123));
            Assert.AreEqual(12312, (long)new BigNumber(12312));
            Assert.AreEqual(123123123, (long)new BigNumber(123123123));
        }

        [TestMethod]
        public void CreateFromString()
        {
            Assert.AreEqual("0", new BigNumber("0").ToString());
            Assert.AreEqual("123", new BigNumber("123").ToString());
            Assert.AreEqual("-123", new BigNumber("-123").ToString());
            Assert.AreEqual("12.312K", new BigNumber("12312").ToString());
            Assert.AreEqual("12,312", new BigNumber("12312").ToString(Unit.Million));
            Assert.AreEqual("12.312M", new BigNumber("12312123").ToString(Unit.Million));
            Assert.AreEqual("12,312,123", new BigNumber("12312123").ToString(Unit.Billion));
            Assert.AreEqual("123.123N", new BigNumber("123123123123123123123123123123123").ToString());
        }

        [TestMethod]
        public void LongFormat()
        {
            Assert.AreEqual("0", new BigNumber("0").ToString(UnitFormat.Full));
            Assert.AreEqual("123", new BigNumber("123").ToString(UnitFormat.Full));
            Assert.AreEqual("12.312 Thousand", new BigNumber("12312").ToString(UnitFormat.Full));
            Assert.AreEqual("123.123 Nonillion", new BigNumber("123123123123123123123123123123123").ToString(UnitFormat.Full));
        }

        [TestMethod]
        public void ArithmeticsPlus()
        {
            Assert.AreEqual("1", new BigNumber(0) + 1);
            Assert.AreEqual("124", new BigNumber(123) + 1);
            Assert.AreEqual("124", 123 + new BigNumber(1));
            Assert.AreEqual("246.246N", new BigNumber("123123123123123123123123123123123") + new BigNumber("123123123123123123123123123123123"));
        }

        [TestMethod]
        public void ArithmeticsMinus()
        {
            Assert.AreEqual("12K", new BigNumber(12312) - 312);
            Assert.AreEqual("12K", 12312 - new BigNumber(312));
            Assert.AreEqual("-12K", new BigNumber(312) - 12312);
            Assert.AreEqual("-12K", 312 - new BigNumber(12312));
            Assert.AreEqual("123N", new BigNumber("123123123123123123123123123123123") - new BigNumber("123123123123123123123123123123"));
            Assert.AreEqual("-123N", new BigNumber("123123123123123123123123123123") - new BigNumber("123123123123123123123123123123123"));
            Assert.AreEqual("0", new BigNumber("123123123123123123123123123123123") - new BigNumber("123123123123123123123123123123123"));
        }

        [TestMethod]
        public void ArithmeticsMultiply()
        {
            Assert.AreEqual("0", new BigNumber(0) * new BigNumber(1));
            Assert.AreEqual("-123", new BigNumber(123) * new BigNumber(-1));
            Assert.AreEqual("123", new BigNumber(123) * new BigNumber(1));
            Assert.AreEqual(new BigNumber("39601209483"), new BigNumber(321123) * new BigNumber(123321));
            Assert.AreEqual(new BigNumber("-39601209483"), new BigNumber(-123321) * new BigNumber(321123));
            Assert.AreEqual(new BigNumber("39601209483"), new BigNumber(-123321) * new BigNumber(-321123));
        }

        [TestMethod]
        public void ArithmeticsMultiplyFloat()
        {
            Assert.AreEqual("0", new BigNumber(0) * 1f);
            Assert.AreEqual("-123", new BigNumber(123) * -1f);
            Assert.AreEqual("123", 123f * new BigNumber(1));
            Assert.AreEqual(new BigNumber("160561"), new BigNumber(321123) * .5f);
            Assert.AreEqual(new BigNumber("160561"), .5f * new BigNumber(321123));
            Assert.AreEqual(new BigNumber("-160561"), new BigNumber(-321123) * .5f);
            Assert.AreEqual(new BigNumber("-160561"), .5f * new BigNumber(-321123));
            Assert.AreEqual(new BigNumber("-160561"), new BigNumber(321123) * -.5f);
            Assert.AreEqual(new BigNumber("-160561"), -.5f * new BigNumber(321123));
            Assert.AreEqual(new BigNumber("160561"), new BigNumber(-321123) * -.5f);
            Assert.AreEqual(new BigNumber("160561"), -.5f * new BigNumber(-321123));
            Assert.AreEqual(new BigNumber("42388"), new BigNumber(321123) * .132f);
            Assert.AreEqual(new BigNumber("42388"), .132f * new BigNumber(321123));
        }

        [TestMethod]
        public void ArithmeticsDivideFloat()
        {
            Assert.AreEqual("0", new BigNumber(0) / 1f);
            Assert.AreEqual("123", new BigNumber(123) / 1f);
            Assert.AreEqual("-123", new BigNumber(123) / -1f);
            Assert.AreEqual("123", new BigNumber(-123) / -1f);
            Assert.AreEqual(new BigNumber("160561"), new BigNumber(321123) / 2);
            Assert.AreEqual(new BigNumber("-160561"), new BigNumber(-321123) / 2);
            Assert.AreEqual(new BigNumber("-160561"), new BigNumber(321123) / -2f);
            Assert.AreEqual(new BigNumber("160561"), new BigNumber(-321123) / -2f);
            Assert.AreEqual(new BigNumber("24701"), new BigNumber(321123) / 13);
        }
    }
}
